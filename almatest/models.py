import asyncio
import random
from dataclasses import dataclass, field
from enum import auto
from typing import Optional, List
from uuid import uuid4

from almatest.config import get_configuration
from almatest.utils.enum import AutoName
from almatest.utils.logger import configure_logger

log = configure_logger()
config = get_configuration()


class Action(AutoName):
    MINE_FOO = auto()
    MINE_BAR = auto()
    ASSEMBLE = auto()
    SELL = auto()
    BUY = auto()
    STOP = auto()


@dataclass
class Robot:
    id: int
    prev_action: Optional[Action] = None
    next_action: Optional[Action] = None


@dataclass
class Stock:
    # foos, bars & foobars
    foos: List[str] = field(default_factory=lambda: [])
    bars: List[str] = field(default_factory=lambda: [])
    foobars: List[dict] = field(default_factory=lambda: [])
    # Money
    funds: int = 0
    # Robots
    robots: List[Robot] = field(default_factory=lambda: [])
    data_lock: asyncio.Lock = field(default_factory=lambda: asyncio.Lock())

    def __post_init__(self):
        for x in range(config.nb_robot_at_start):
            self.robots.append(Robot(x))

    def can_buy(self) -> bool:
        return self.funds >= 3 and len(self.foos) >= 6 \
            and len(self.robots) < config.nb_robots_to_achieve

    def can_sell(self) -> bool:
        return len(self.foobars) > 0

    async def change_activity_delay_if_needed(self, robot_id, next_action):
        if self.robots[robot_id].prev_action is not None \
                and self.robots[robot_id].prev_action != next_action:
            log.info(f"[STEP MOVE] for robot {robot_id}")
            await asyncio.sleep(5 * config.one_play_time)

    async def mine_foo(self, robot_id: int):
        await asyncio.sleep(config.one_play_time)
        self.robots[robot_id].prev_action = Action.MINE_FOO
        self.robots[robot_id].next_action = None
        self.foos.append(str(uuid4()))

    async def mine_bar(self, robot_id: int):
        mine_bar_time = round(random.uniform(0.5, 2), 1) * config.one_play_time
        await asyncio.sleep(mine_bar_time)
        self.robots[robot_id].prev_action = Action.MINE_BAR
        self.robots[robot_id].next_action = None
        self.bars.append(str(uuid4()))

    async def assemble(self, robot_id: int):
        await asyncio.sleep(2 * config.one_play_time)
        a_foo = self.foos[0]
        a_bar = self.bars[0]
        self.robots[robot_id].prev_action = Action.ASSEMBLE
        self.robots[robot_id].next_action = None
        success = random.randrange(100) < config.assemble_success_percent
        if success:
            self.foobars.append({"foo": a_foo, "bar": a_bar})
            self.foos.remove(a_foo)
            self.bars.remove(a_bar)
        else:
            self.foos.remove(a_foo)

    async def sell_foobar(self, robot_id: int):
        await asyncio.sleep(10 * config.one_play_time)
        self.robots[robot_id].prev_action = Action.SELL
        self.robots[robot_id].next_action = None
        if self.can_sell():
            nb_foobar_to_sell = len(self.foobars)
            if nb_foobar_to_sell > 5:
                nb_foobar_to_sell = 5
            self.funds += nb_foobar_to_sell
            self.foobars = self.foobars[nb_foobar_to_sell:]

    def buy_robot(self, robot_id: int):
        self.robots[robot_id].prev_action = Action.BUY
        self.robots[robot_id].next_action = None
        if self.can_buy():
            robot = Robot(id=(len(self.robots)))
            self.robots.append(robot)
            self.funds -= 3
            self.foobars = self.foobars[6:]
            return robot

    def __repr__(self):
        return f"[Stock] foo(s) {len(self.foos)} " \
               f"bar(s): {len(self.bars)} " \
               f"foobar(s): {len(self.foobars)} " \
               f"funds: {self.funds} " \
               f"robots: {len(self.robots)}"
