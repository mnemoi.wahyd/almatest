from dataclasses import dataclass, field

DEFAULT_SUCCESS_PERCENT = 60
DEFAULT_ROBOTS_NB_AT_START = 2
DEFAULT_ROBOTS_NB_TO_ACHIEVE = 30
ONE_PLAY_TIME = 0.005


@dataclass
class Config:
    assemble_success_percent: int = field(default_factory=lambda: DEFAULT_SUCCESS_PERCENT)
    nb_robot_at_start: int = field(default_factory=lambda: DEFAULT_ROBOTS_NB_AT_START)
    nb_robots_to_achieve: int = field(default_factory=lambda: DEFAULT_ROBOTS_NB_TO_ACHIEVE)
    one_play_time: float = field(default_factory=lambda: ONE_PLAY_TIME)


def get_configuration():
    return Config()
