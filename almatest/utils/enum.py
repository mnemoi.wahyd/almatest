from enum import Enum


class AutoName(Enum):
    """Automatically set the enum to its name in lower case."""

    @staticmethod
    def _generate_next_value_(name, _start, _count, _last_values):
        return name
