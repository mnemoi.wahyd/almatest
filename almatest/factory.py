import asyncio

from almatest.config import get_configuration
from almatest.models import Action, Stock, Robot
from almatest.utils.logger import configure_logger

log = configure_logger()
config = get_configuration()


class Factory:

    def __init__(self):
        self.stock = Stock()

    async def run_production_chain(self):
        log.info("Start production chain")
        await asyncio.gather(*[self.schedule_next_action(robot) for robot in self.stock.robots])
        log.info(f"[STOCK STATE AT THE END] {self.stock!r}")
        log.info("End production chain")

    async def schedule_next_action(self, robot: Robot):
        async with self.stock.data_lock:
            new_robot = None
            next_action: Action = self.determine_next_action()

            if next_action == Action.STOP:
                log.debug(f"[STOP STEP] end for robot {robot.id}")
                return

            await self.stock.change_activity_delay_if_needed(robot.id, next_action)

            log.debug(f"[ACTION STEP] robot {robot.id} is gonna {next_action.name}")

            if next_action == Action.MINE_FOO:
                await self.stock.mine_foo(robot.id)

            if next_action == Action.MINE_BAR:
                await self.stock.mine_bar(robot.id)

            if next_action == Action.ASSEMBLE:
                await self.stock.assemble(robot_id=robot.id)

            if next_action == Action.SELL:
                await self.stock.sell_foobar(robot.id)

            if next_action == Action.BUY:
                new_robot = self.stock.buy_robot(robot.id)
                nb_robots = len(self.stock.robots)
                if nb_robots >= config.nb_robots_to_achieve:
                    log.debug(f"[STOP STEP] for robot {robot.id}")
                    return

            log.info(f"[STOCK STATE] {self.stock!r}")

        if new_robot:
            asyncio.create_task(self.schedule_next_action(robot=new_robot))

        await self.schedule_next_action(robot=robot)

    def determine_next_action(self):
        no_foos = len(self.stock.foos) == 0
        no_bars = len(self.stock.bars) == 0
        nb_robots = len(self.stock.robots)
        nb_foos = len(self.stock.foos)

        if nb_robots >= config.nb_robots_to_achieve:
            return Action.STOP
        if self.stock.can_buy():
            return Action.BUY
        if self.stock.can_sell():
            return Action.SELL
        if no_foos:
            return Action.MINE_FOO
        if no_bars:
            return Action.MINE_BAR
        if nb_foos < 6:
            return Action.MINE_FOO
        return Action.ASSEMBLE
