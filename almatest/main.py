import asyncio

from almatest.config import get_configuration
from almatest.factory import Factory
from almatest.utils.logger import configure_logger

log = configure_logger()
config = get_configuration()


def app():
    factory = Factory()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(factory.run_production_chain())


if __name__ == "__main__":
    app()
