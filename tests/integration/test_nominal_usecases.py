from almatest.config import get_configuration


def test_robots_number_achieved(stock, after_run_production_chain):
    """ We want to check if we reach <nb_robots_to_achieve>"""
    config = get_configuration()
    assert len(stock.robots) == config.nb_robots_to_achieve
    # the last one has not been used for sure
    assert stock.robots[config.nb_robots_to_achieve - 1].prev_action is None
    assert stock.robots[config.nb_robots_to_achieve - 1].next_action is None
