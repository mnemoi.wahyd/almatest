import asyncio

import pytest

from almatest.factory import Factory
from almatest.utils.logger import configure_logger

log = configure_logger()


@pytest.fixture
def factory():
    factory = Factory()
    return factory


@pytest.fixture
def stock(factory):
    return factory.stock


@pytest.fixture
@pytest.mark.asyncio
async def after_run_production_chain(factory):
    await factory.run_production_chain()
    yield
    tasks = [
        task for task in asyncio.all_tasks() if task is not asyncio.current_task()
    ]
    list(map(lambda task: task.cancel(), tasks))
    await asyncio.gather(*tasks, return_exceptions=True)
