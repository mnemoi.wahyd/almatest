import asyncio
from uuid import uuid4

from almatest.config import get_configuration


def test_stock(stock):
    config = get_configuration()
    assert len(stock.robots) == config.nb_robot_at_start
    assert len(stock.foos) == 0
    assert len(stock.bars) == 0
    assert len(stock.foobars) == 0


def test_sell_foobar(stock):
    """ Because our algorithm-ish sell a foobar once it is ready,
    here a test to check the rule to sell max 5 foobars """

    # Setup
    for _ in range(10):
        stock.foobars.append({"foo": uuid4(), "bar": uuid4()})

    # Run
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(stock.sell_foobar(stock.robots[0].id))

    # Assert
    assert stock.funds == 5
    assert len(stock.foobars) == 5

    # Close
    loop.close()
