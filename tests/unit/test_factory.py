from almatest.config import get_configuration


def test_factory(factory):
    config = get_configuration()
    assert len(factory.stock.robots) == config.nb_robot_at_start
    assert len(factory.stock.foos) == 0
    assert len(factory.stock.bars) == 0
    assert len(factory.stock.foobars) == 0
