Alma Test
===========

Dependencies
------------

* [pyenv](https://github.com/pyenv/pyenv#installation)
* [Poetry](https://python-poetry.org/docs/#installation)

Getting started
-----

```bash
pyenv install 3.10
```

After cloning the repo to run the application

```bash
poetry run foobartory -vvv
```

to run the tests & coverage
```bash
poetry run coverage run -m pytest && poetry run coverage report -m
```